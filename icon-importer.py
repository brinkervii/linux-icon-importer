#!/usr/bin/env python3
import re
import sys
from mimetypes import guess_extension
from pathlib import Path
from typing import Optional, Tuple, List

import click
import magic
from PIL import Image

icon_size_pattern = re.compile(r"(\d+)x(\d+)")


def parse_icon_size(file: Path) -> Optional[Tuple[int, int]]:
    if not file.is_dir():
        return None

    if match := icon_size_pattern.search(file.name):
        return int(match.group(1)), int(match.group(2))

    return None


@click.command()
@click.argument("input-icon", type=Path)
@click.argument("icon-theme", type=Path)
@click.argument("icon-name", type=str)
def main(input_icon: Path, icon_theme: Path, icon_name: str):
    assert input_icon.exists(), "Input icon does not exist"
    assert input_icon.is_file(), "Input icon is not a file"

    icon_sizes: List[Tuple[int, int]] = list(
        filter(
            None,
            map(
                parse_icon_size,
                icon_theme.glob("*")
            )
        )
    )

    image_mime_type = magic.from_file(input_icon, mime=True)
    image_extension = guess_extension(image_mime_type)

    with Image.open(input_icon) as input_image:
        for size in icon_sizes:
            target_path = icon_theme / ("x".join(list(map(str, size)))) / "apps" / (icon_name + image_extension)
            print(f"Writing icon to {target_path}", file=sys.stderr)

            with input_image.resize(size, Image.BICUBIC) as resized_icon:
                resized_icon.save(target_path)

    print(f"Done!", file=sys.stderr)


if __name__ == "__main__":
    main()
